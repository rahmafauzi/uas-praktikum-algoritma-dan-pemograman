/*
Nama : Rahma Fauzi Nurul Islam
Kelas : IF-1A
NIM : 301230056
Soal No 1
*/
#include <iostream>
#include "myheader.h"
using namespace std;
int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;
    quiz = 40; absen = 100; uts = 60; uas = 50; tugas = 80;
    tampilkanHasil(absen, tugas, quiz, uts, uas);
    nilai = ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;
    Huruf_Mutu = hitungHurufMutu(nilai);
    cout << "Huruf Mutu : " << Huruf_Mutu) << endl;
    return 0;
}

#include <iostream>
#include "myheader"

using namespace std;

char tentukanHurufMutu(double nilai) {
    char hurufMutu;

    if (nilai > 85 && nilai <= 100)
        hurufMutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        hurufMutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        hurufMutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        hurufMutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        hurufMutu = 'E';

    return hurufMutu;
}

void tampilkanHasil(double absen, double tugas, double quiz, double uts, double uas) {
    cout << "Absen = " << absen << endl;
    cout << "Tugas = " << tugas << endl;
    cout << "Quiz = " << quiz << endl;
    cout << "UTS = " << uts << endl;
    cout << "UAS = " << uas << endl;
}
